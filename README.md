# Demo test using confluent kafka via rest api and kafkajs.

Start services like `npm run boot:order-service`

## For REST API use
### .env params needed
- BASIC_TOKEN := base64 encoding of string "confluent_api_key:confluent_api_secret"
- CONFLUENT_REST_API_URL := base url of confluent rest api v3
- CLUSTER_ID := main cluster id to target

## For KafkaJS use
### .env params needed
- BOOT_STRAP_URL := Confluent Bootstrap server endpoint url
- USER_NAME := Confluent API Key
- PASSWORD := Confluent API Key's Secret