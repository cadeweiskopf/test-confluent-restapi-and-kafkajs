import { Client } from "./utils/clientClass";
import { randomUUID } from "crypto";

const numberOfClients = 3;

const clients = [];

for (let i = 0; i < numberOfClients; i++) {
  const randomClientId = randomUUID();
  const client = new Client(randomClientId);
  clients.push(client);
}
