import {
  BOOT_STRAP_URL,
  USER_NAME,
  PASSWORD,
} from "./utils/environment-variables";
import { newConsumer, newKafka } from "./utils/kafka-stuff";

const kafka = newKafka(BOOT_STRAP_URL, USER_NAME, PASSWORD);
const consumer = newConsumer(
  kafka,
  "cw-test-group",
  () => {
    console.log(`Kafka Consumer: connected`);
  },
  () => {
    console.log(`Kafka Consumer: could not connect`);
  },
  (payload) => {
    console.log(`Kafka Consumer: request timeout ${payload.payload.clientId}`);
  }
);

const startConsumer = async () => {
  await consumer.connect();
  await consumer.subscribe({ topics: ["new_test_topic"], fromBeginning: true });
  await consumer.run({
    eachMessage: async ({ topic, partition, message, heartbeat, pause }) => {
      console.log(
        {
          topic,
          partition,
          key: message.key?.toString(),
          value: message.value?.toString(),
          headers: message.headers,
        },
        "\n"
      );
    },
  });
};
startConsumer();
