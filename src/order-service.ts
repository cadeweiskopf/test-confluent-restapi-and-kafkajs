import express from "express";
import {
  newKafka,
  newProducer,
  postToTopicViaKafkaJs,
  postToTopicViaRestApi,
} from "./utils/kafka-stuff";
import { randomUUID } from "crypto";
import cookieParser from "cookie-parser";
import http from "http";
import { IWsClient, newWebsocketServer } from "./utils/websocket-stuff";
import {
  BOOT_STRAP_URL,
  USER_NAME,
  PASSWORD,
  CONFLUENT_REST_API_URL,
  BASIC_TOKEN,
  CLUSTER_ID,
  ORDER_SERVICE_PORT,
} from "./utils/environment-variables";

// kafka
const kafka = newKafka(BOOT_STRAP_URL, USER_NAME, PASSWORD);
const producer = newProducer(
  kafka,
  () => {
    console.log(`KafkaProvider: connected`);
  },
  () => {
    console.log(`KafkaProvider: could not connect`);
  },
  (payload) => {
    console.log(`KafkaProvider: request timeout ${payload.payload.clientId}`);
  }
);

// express
const app = express();

const setExpressAppMiddleware = () => {
  app.use(cookieParser());
  const appMiddlewareForKafkaKey = (req, res, next) => {
    const { kafkaKey } = req.cookies;
    if (!kafkaKey) {
      console.log(`!kafaKey in cookies, setting...`);
      const randomUuid = randomUUID();
      res.cookie("kafkaKey", randomUuid);
    }
    next();
  };
  app.use(appMiddlewareForKafkaKey);
};
setExpressAppMiddleware();

const setExpressAppEndpoints = () => {
  app.get("/", async (req, res) => {
    const { kafkaKey } = req.cookies;
    const topic = "new_test_topic";
    const message = `Hello World ${new Date().getTime()}`;
    const data = await postToTopicViaRestApi(
      CONFLUENT_REST_API_URL,
      BASIC_TOKEN,
      CLUSTER_ID,
      topic,
      message,
      kafkaKey
    );
    res.send(`Sent via REST API: ${message} --> ${data}`);
  });

  app.get("/kafkajs", async (req, res) => {
    const { kafkaKey } = req.cookies;
    const topic = "new_test_topic";
    const message = `KafkaJS Hello World ${new Date().getTime()}`;
    const data = await postToTopicViaKafkaJs(
      producer,
      topic,
      message,
      kafkaKey
    );
    res.send(`Sent via KafkaJS: ${message} --> ${data}`);
  });
};
setExpressAppEndpoints();

const server = http.createServer(app);
server.listen(ORDER_SERVICE_PORT, async () => {
  console.log("listening");
  await producer.connect();
  return console.log(`Order service listening on ${ORDER_SERVICE_PORT}`);
});

// websocket
const wsClients: IWsClient[] = [];
const wsServer = newWebsocketServer(server, wsClients);

const periodicallyMessageAllClients = () => {
  wsClients.forEach((wsClient) => {
    wsClient.connection.sendUTF("Hello world");
  });
  setTimeout(periodicallyMessageAllClients, 5000);
};
periodicallyMessageAllClients();