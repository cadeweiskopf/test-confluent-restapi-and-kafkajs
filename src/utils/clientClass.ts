import { client as websocketClient, Message } from "websocket";
import { ORDER_SERVICE_PORT } from "./environment-variables";

export class Client {
  private static randomMessages(client: websocketClient, ) {
    const sendRandomMessage = () => {};
    sendRandomMessage();
  }

  private client: websocketClient;
  private clientId: string;

  private handleMessage = (message: Message) => {
    if (message.type === "utf8") {
      console.log(`${this.clientId} New Message: ${message.utf8Data}`);
    } else if (message.type === "binary") {
      console.log(
        `${this.clientId} New binary message: ${message.binaryData.length} bytes`
      );
    }
  };

  constructor(id: string) {
    this.client = new websocketClient();
    this.clientId = id;
    console.log(`New client instantiated: ${this.clientId}`);

    this.client.on("connectFailed", (error) => {
      console.log(
        `${this.clientId} Client Connect Failed Error: ${error.toString()}`
      );
    });

    this.client.on("connect", (connection) => {
      console.log(`${this.clientId} WebSocket Client Connected`);

      connection.on("error", (error) => {
        console.log(`${this.clientId} Connection Error: ${error.toString()}`);
      });

      connection.on("close", () => {
        console.log(`${this.clientId} echo-protocol Connection Closed`);
      });

      connection.on("message", this.handleMessage);

      Client.randomMessages(this.client);
    });

    this.client.connect(
      `ws://localhost:${ORDER_SERVICE_PORT}/`,
      "echo-protocol"
    );
  }
}
