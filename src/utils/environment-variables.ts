const {
  BASIC_TOKEN,
  CONFLUENT_REST_API_URL,
  CLUSTER_ID,
  BOOT_STRAP_URL,
  USER_NAME,
  PASSWORD,
  ORDER_SERVICE_PORT,
  INVENTORY_SERVICE_PORT,
  CLIENT_PORT,
} = process.env;

if (!BASIC_TOKEN) {
  throw Error(`Missing env var: BASIC_TOKEN`);
}
export { BASIC_TOKEN };

if (!CONFLUENT_REST_API_URL) {
  throw Error(`Missing env var: CONFLUENT_REST_API_URL`);
}
export { CONFLUENT_REST_API_URL };

if (!CLUSTER_ID) {
  throw Error(`Missing env var: CLUSTER_ID`);
}
export { CLUSTER_ID };

if (!BOOT_STRAP_URL) {
  throw Error(`Missing env var: BOOT_STRAP_URL`);
}
export { BOOT_STRAP_URL };

if (!USER_NAME) {
  throw Error(`Missing env var: USER_NAME`);
}
export { USER_NAME };

if (!PASSWORD) {
  throw Error(`Missing env var: PASSWORD`);
}
export { PASSWORD };

if (!ORDER_SERVICE_PORT) {
  throw Error(`Missing env var: ORDER_SERVICE_PORT`);
}
export { ORDER_SERVICE_PORT };

if (!INVENTORY_SERVICE_PORT) {
  throw Error(`Missing env var: INVENTORY_SERVICE_PORT`);
}
export { INVENTORY_SERVICE_PORT };

if (!CLIENT_PORT) {
  throw Error(`Missing env var: CLIENT_PORT`);
}
export { CLIENT_PORT };
