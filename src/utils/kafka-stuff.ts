import axios from "axios";
import crypto from "crypto";
import https from "https";
import { Consumer, Kafka, Producer, RequestTimeoutEvent } from "kafkajs";

const ssl = {
  // for self signed
  rejectUnauthorized: false,
  // allow legacy server
  secureOptions: crypto.constants.SSL_OP_LEGACY_SERVER_CONNECT,
};

export const newKafka = (
  bootStrapUrl: string,
  username: string,
  password: string
): Kafka => {
  const kafka = new Kafka({
    clientId: "cw-test-kafkajs",
    brokers: [bootStrapUrl],
    ssl,
    logLevel: 2,
    sasl: {
      mechanism: "plain",
      username,
      password,
    },
  });
  return kafka;
};

export const newProducer = (
  kafka: Kafka,
  onConnect: () => void,
  onDisconnect: () => void,
  onRequestTimeout: (payload: RequestTimeoutEvent) => void
): Producer => {
  const producer = kafka.producer();
  producer.on("producer.connect", onConnect);
  producer.on("producer.disconnect", onDisconnect);
  producer.on("producer.network.request_timeout", onRequestTimeout);
  return producer;
};

export const newConsumer = (
  kafka: Kafka,
  groupId: string,
  onConnect: () => void,
  onDisconnect: () => void,
  onRequestTimeout: (payload: RequestTimeoutEvent) => void
): Consumer => {
  const consumer = kafka.consumer({ groupId });
  consumer.on("consumer.connect", onConnect);
  consumer.on("consumer.disconnect", onDisconnect);
  consumer.on("consumer.network.request_timeout", onRequestTimeout);
  return consumer;
};

export const postToTopicViaKafkaJs = async (
  producer: Producer,
  topic: string,
  message: string,
  key: string
): Promise<string> => {
  try {
    const response = await producer.send({
      topic,
      messages: [{ key, value: message }],
    });
    const responseData = JSON.stringify(response);
    return responseData;
  } catch (error) {
    console.log(error);
    return JSON.stringify(error);
  }
};

export const postToTopicViaRestApi = async (
  apiBaseUrl: string,
  basicToken: string,
  clusterId: string,
  topic: string,
  message: string,
  key: string
): Promise<string> => {
  const data = JSON.stringify({
    key: { type: "STRING", data: key },
    value: {
      type: "JSON",
      data: message,
    },
  });

  const config = {
    httpsAgent: new https.Agent(ssl),
    method: "post",
    maxBodyLength: Infinity,
    url: `${apiBaseUrl}/clusters/${clusterId}/topics/${topic}/records`,
    headers: {
      Authorization: `Basic ${basicToken}`,
      "Content-Type": "application/json",
    },
    data,
  };

  try {
    const response = await axios.request(config);
    const responseData = JSON.stringify(response.data);
    return responseData;
  } catch (error) {
    console.log(error);
    return JSON.stringify(error.message);
  }
};
